# configuration-utils

# About

A library used for parsing configurations for (JVM based) applications from multiple sources (files, command line, environment). Also, it creates a simple, self-documented CLI in standard format based on descriptions of configuration fields.

# Requirements

Minimal requirements for this library are:

- Java 8 or higher
- Maven or other build tools compatible with Maven (e.g. Gradle)

# Usage

To install the library, open the shell in the project root folder and run install command.

```terminal
mvn clean install
```

For quickstart and detailed usage, please consider [this](https://gitlab.com/dataflux_group/backend/stream-processing-utils/configuration-utils/-/wikis/home) link. 