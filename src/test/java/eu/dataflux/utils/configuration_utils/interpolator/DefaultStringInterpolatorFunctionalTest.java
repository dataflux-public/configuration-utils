package eu.dataflux.utils.configuration_utils.interpolator;

import eu.dataflux.utils.configuration_utils.interpolator.impl.DefaultStringInterpolator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class DefaultStringInterpolatorFunctionalTest {

    @Test
    void interpolateSimple() {
        IStringInterpolator unit = new DefaultStringInterpolator();

        String sample = "foo=${SOME_VALUE}";
        Map<String, String> values = new HashMap<>();
        values.put("SOME_VALUE", "dsa");

        String result = unit.interpolate(sample, values);
        Assertions.assertEquals("foo=dsa", result);
    }

    @Test
    void interpolateWithDefaultValue_ValueProvided() {
        IStringInterpolator unit = new DefaultStringInterpolator();

        String sample = "foo=${SOME_VALUE:-dafv}";
        Map<String, String> values = new HashMap<>();
        values.put("SOME_VALUE", "dsa");

        String result = unit.interpolate(sample, values);
        Assertions.assertEquals("foo=dsa", result);
    }

    @Test
    void interpolateWithDefaultValue_ValueNotProvided() {
        IStringInterpolator unit = new DefaultStringInterpolator();

        String sample = "foo=${SOME_VALUE:-dafv}";
        Map<String, String> values = new HashMap<>();

        String result = unit.interpolate(sample, values);
        Assertions.assertEquals("foo=dafv", result);
    }
}
