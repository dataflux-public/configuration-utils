package eu.dataflux.utils.configuration_utils.cli;

import eu.dataflux.utils.configuration_utils.Configuration;
import eu.dataflux.utils.configuration_utils.cli.impl.DefaultCommandLine;
import eu.dataflux.utils.configuration_utils.configuration_samples.SampleConfigurations;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.JsonConfigurationParser;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CliFunctionalTests {

    @Test
    void parseIntegerConfiguration() {
        String[] args = new String[]{"-f", "20", "-b", "30"};
        ICommandLine cmd = createCLI(SampleConfigurations.IntegerConfiguration.class, args);

        SampleConfigurations.IntegerConfiguration conf = new SampleConfigurations.IntegerConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals(20, conf.getFoo());
        assertEquals(30, conf.getBar());
    }

    @Test
    void parseInvalidIntegerConfiguration() {
        String[] args = new String[]{"-f", "20sd", "-b", "3a0"};
        ICommandLine cmd = createCLI(SampleConfigurations.IntegerConfiguration.class, args);

        SampleConfigurations.IntegerConfiguration conf = new SampleConfigurations.IntegerConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNull(conf.getFoo());
        assertEquals(0, conf.getBar());
    }

    @Test
    void parseLongConfiguration() {
        String[] args = new String[]{"-f", "20", "-b", "30"};
        ICommandLine cmd = createCLI(SampleConfigurations.LongConfiguration.class, args);

        SampleConfigurations.LongConfiguration conf = new SampleConfigurations.LongConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals(20L, conf.getFoo());
        assertEquals(30L, conf.getBar());
    }

    @Test
    void parseInvalidLongConfiguration() {
        String[] args = new String[]{"-f", "20sd", "-b", "3a0"};
        ICommandLine cmd = createCLI(SampleConfigurations.LongConfiguration.class, args);

        SampleConfigurations.LongConfiguration conf = new SampleConfigurations.LongConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNull(conf.getFoo());
        assertEquals(0L, conf.getBar());
    }

    @Test
    void parseDoubleConfiguration() {
        String[] args = new String[]{"-f", "20.5", "-b", "30.5"};
        ICommandLine cmd = createCLI(SampleConfigurations.DoubleConfiguration.class, args);

        SampleConfigurations.DoubleConfiguration conf = new SampleConfigurations.DoubleConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals(20.5, conf.getFoo());
        assertEquals(30.5, conf.getBar());
    }

    @Test
    void parseInvalidDoubleConfiguration() {
        String[] args = new String[]{"-f", "20.3dsa5", "-b", "30as.5"};
        ICommandLine cmd = createCLI(SampleConfigurations.DoubleConfiguration.class, args);

        SampleConfigurations.DoubleConfiguration conf = new SampleConfigurations.DoubleConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNull(conf.getFoo());
        assertEquals(0, conf.getBar());
    }

    @Test
    void parseFloatConfiguration() {
        String[] args = new String[]{"-f", "20.5", "-b", "30.5"};
        ICommandLine cmd = createCLI(SampleConfigurations.FloatConfiguration.class, args);

        SampleConfigurations.FloatConfiguration conf = new SampleConfigurations.FloatConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals(20.5f, conf.getFoo());
        assertEquals(30.5f, conf.getBar());
    }

    @Test
    void parseInvalidFloatConfiguration() {
        String[] args = new String[]{"-f", "20.3dsa5", "-b", "30as.5"};
        ICommandLine cmd = createCLI(SampleConfigurations.FloatConfiguration.class, args);

        SampleConfigurations.FloatConfiguration conf = new SampleConfigurations.FloatConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNull(conf.getFoo());
        assertEquals(0f, conf.getBar());
    }

    @Test
    void parseBooleanConfiguration() {
        String[] args = new String[]{"-f", "true", "-b", "false"};
        ICommandLine cmd = createCLI(SampleConfigurations.BooleanConfiguration.class, args);

        SampleConfigurations.BooleanConfiguration conf = new SampleConfigurations.BooleanConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertTrue(conf.getFoo());
        assertFalse(conf.getBar());
    }

    @Test
    void parseInvalidBooleanConfiguration() {
        String[] args = new String[]{"-f", "20.3dsa5", "-b", "30as.5"};
        ICommandLine cmd = createCLI(SampleConfigurations.BooleanConfiguration.class, args);

        SampleConfigurations.BooleanConfiguration conf = new SampleConfigurations.BooleanConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertFalse(conf.getFoo());
        assertFalse(conf.getBar());
    }

    @Test
    void parseStringConfiguration() {
        String[] args = new String[]{"-f", "true"};
        ICommandLine cmd = createCLI(SampleConfigurations.StringConfiguration.class, args);

        SampleConfigurations.StringConfiguration conf = new SampleConfigurations.StringConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals("true", conf.getFoo());
    }

    @Test
    void parseListConfiguration() {
        String[] args = new String[]{
                "-f", "[\"true\", \"sda\"]",
                "-b", "[2,3,5,6]",
                "-c", "[{\"a\":2, \"b\":\"Cpx\"},{\"a\":3, \"b\":\"Fsfpx\"}]"};
        ICommandLine cmd = createCLI(SampleConfigurations.ListConfiguration.class, args);

        SampleConfigurations.ListConfiguration conf = new SampleConfigurations.ListConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNotNull(conf.getFoo());
        assertNotNull(conf.getBar());
        assertNotNull(conf.getComplex());

        assertArrayEquals(new String[]{"true", "sda"}, conf.getFoo().toArray(new String[2]));
        assertArrayEquals(new Integer[]{2, 3, 5, 6}, conf.getBar().toArray(new Integer[2]));
        assertArrayEquals(
                new Object[]{
                        new SampleConfigurations.InnerConfiguration(2, "Cpx"),
                        new SampleConfigurations.InnerConfiguration(3, "Fsfpx")
                }, conf.getComplex().toArray());
    }

    @Test
    void parseInvalidListConfiguration() {
        String[] args = new String[]{
                "-f", "[\"true\", \"sda\"]",
                "-b", "[f2,\"3\",a5,6]",
                "-c", "[{{}\"a\":2, \"b\":\"Cpx\"},{\"a\":3, \"b\":\"Fsfpx\"}]"};
        ICommandLine cmd = createCLI(SampleConfigurations.ListConfiguration.class, args);

        SampleConfigurations.ListConfiguration conf = new SampleConfigurations.ListConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertArrayEquals(new String[]{"true", "sda"}, conf.getFoo().toArray(new String[2]));
        assertNull(conf.getBar());
        assertNull(conf.getComplex());
    }

    @Test
    void parseMapConfiguration() {
        String[] args = new String[]{
                "-f", "{\"true\": \"false\", \"sda\": \"23\"}",
                "-b", "{\"saf\": 25, \"sfg\":34}",
                "-c", "{\"sdsa\": {\"a\":2, \"b\":\"Cpx\"}, \"dsaf\": {\"a\":3, \"b\":\"Fsfpx\"}}"};
        ICommandLine cmd = createCLI(SampleConfigurations.MapConfiguration.class, args);

        SampleConfigurations.MapConfiguration conf = new SampleConfigurations.MapConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertNotNull(conf.getFoo());
        assertNotNull(conf.getBar());
        assertNotNull(conf.getComplex());

        assertTrue(conf.getFoo().containsKey("true"));
        assertTrue(conf.getFoo().containsKey("sda"));
        assertEquals("false", conf.getFoo().get("true"));
        assertEquals("23", conf.getFoo().get("sda"));

        assertTrue(conf.getBar().containsKey("saf"));
        assertTrue(conf.getBar().containsKey("sfg"));
        assertEquals(25, conf.getBar().get("saf"));
        assertEquals(34, conf.getBar().get("sfg"));

        assertTrue(conf.getComplex().containsKey("sdsa"));
        assertTrue(conf.getComplex().containsKey("dsaf"));
        assertEquals(new SampleConfigurations.InnerConfiguration(2, "Cpx"), conf.getComplex().get("sdsa"));
        assertEquals(new SampleConfigurations.InnerConfiguration(3, "Fsfpx"), conf.getComplex().get("dsaf"));
    }

    @Test
    void parseComplexConfiguration() {
        String[] args = new String[]{"-f", "20", "-b", "30", "-a", "string", "-c", "32"};
        ICommandLine cmd = createCLI(SampleConfigurations.ComplexSampleConfiguration.class, args);

        SampleConfigurations.ComplexSampleConfiguration conf = new SampleConfigurations.ComplexSampleConfiguration();
        cmd.overrideWithCliOptions(conf);

        assertEquals("string", conf.getaFoo());
        assertEquals(32, conf.getcFar());
        assertNotNull(conf.getInteger());
        assertEquals(20, conf.getInteger().getFoo());
        assertEquals(30, conf.getInteger().getBar());
    }

    @Test
    void displayHelpIfRequestedForComplexConfiguration() {
        ICommandLine cmd = createCLI(SampleConfigurations.ComplexSampleConfiguration.class, new String[]{"-h"});

        PrintStream oldPs = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bos, true);
        System.setOut(ps);

        assertTrue(cmd.isHelpRequested());
        cmd.displayHelp();

        System.setOut(oldPs);
        String actualOutput = new String(bos.toByteArray(), StandardCharsets.UTF_8).trim();
        String expectedOutput = "usage: eu.dataflux.utils.configuration_utils.cli.CliFunctionalTests " +
                "[-a <arg>] [-b <arg>] [-c <arg>] [-f <arg>] [-h]" + System.lineSeparator() +
                "Test header" + System.lineSeparator() +
                " -a,--aFoo <arg>   aFoo sample." + System.lineSeparator() +
                " -b,--bar <arg>    Bar sample." + System.lineSeparator() +
                " -c,--cFar <arg>   cFar sample." + System.lineSeparator() +
                " -f,--foo <arg>    Foo sample." + System.lineSeparator() +
                " -h,--help         Preview help" + System.lineSeparator() +
                "Test footer";

        assertEquals(expectedOutput, actualOutput);
    }

    private ICommandLine createCLI(Class<?> tClass, String[] arguments) {
        ICommandLine cmd = new DefaultCommandLine.Builder()
                .defaultFooter("footer")
                .defaultHeader("header")
                .complexArgumentParser(new JsonConfigurationParser())
                .build();

        cmd.initialize(CliFunctionalTests.class, arguments,
                Collections.singletonList(new Configuration.Builder()
                        .template(tClass)
                        .build())
        );

        return cmd;
    }
}
