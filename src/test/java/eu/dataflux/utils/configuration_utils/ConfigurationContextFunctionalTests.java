package eu.dataflux.utils.configuration_utils;

import eu.dataflux.utils.configuration_utils.cli.impl.DefaultCommandLine;
import eu.dataflux.utils.configuration_utils.configuration_samples.SampleConfigurations;
import eu.dataflux.utils.configuration_utils.interpolator.impl.DefaultStringInterpolator;
import eu.dataflux.utils.configuration_utils.parser.factory.impl.JacksonParserFactory;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationContextFunctionalTests {

    @Test
    void completeFlowTestRequestHelp() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/integerConfiguration.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .parserFactory(new JacksonParserFactory())
                .commandLine(new DefaultCommandLine.Builder().build())
                .stringInterpolator(new DefaultStringInterpolator())
                .commandLineArguments(new String[]{"-h"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        assertEquals(1, ctx.getConfigurations().size());
        assertEquals(intConfiguration, ctx.getConfigurations().get(0));
        assertTrue(ctx.getEnvironment().isEmpty());
        assertTrue(ctx.getCommandLine().isHelpRequested());

        PrintStream oldPs = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bos, true);
        System.setOut(ps);

        ctx.getCommandLine().displayHelp();

        System.setOut(oldPs);
        String actualOutput = new String(bos.toByteArray(), StandardCharsets.UTF_8).trim();
        String expectedOutput = "usage: eu.dataflux.utils.configuration_utils.ConfigurationContextFunctionalTests " +
                "[-b <arg>] [-cic <arg>] [-f <arg>] [-h]" + System.lineSeparator() +
                "Test header" + System.lineSeparator() +
                " -b,--bar <arg>                      Bar sample." + System.lineSeparator() +
                " -cic,--integerConfiguration <arg>   Sample description" + System.lineSeparator() +
                " -f,--foo <arg>                      Foo sample." + System.lineSeparator() +
                " -h,--help                           Preview help" + System.lineSeparator() +
                "Test footer";

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void completeFlowTestRequestHelpDefaults() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .hasCliOption(true)
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[]{"-h"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        assertTrue(ctx.getCommandLine().isHelpRequested());

        PrintStream oldPs = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bos, true);
        System.setOut(ps);

        ctx.getCommandLine().displayHelp();

        System.setOut(oldPs);
        String actualOutput = new String(bos.toByteArray(), StandardCharsets.UTF_8).trim();
        String expectedOutput = "usage: eu.dataflux.utils.configuration_utils.ConfigurationContextFunctionalTests " +
                "[-b <arg>] [-cic <arg>] [-f <arg>] [-h]" + System.lineSeparator() +
                "Test header" + System.lineSeparator() +
                " -b,--bar <arg>                              Bar sample." + System.lineSeparator() +
                " -cic,--integerConfigurationLocation <arg>   Location of IntegerConfiguration configuration file." + System.lineSeparator() +
                "                                             `integerConfiguration.json` file from jar will be read by default." + System.lineSeparator() +
                " -f,--foo <arg>                              Foo sample." + System.lineSeparator() +
                " -h,--help                                   Preview help" + System.lineSeparator() +
                "Test footer";

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void completeFlowTestReadFromFileInJar() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/integerConfiguration.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[0])
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(2, conf.getFoo());
        assertEquals(3, conf.getBar());
    }

    @Test
    void completeFlowTestReadFromFileInJarDefaults() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .hasCliOption(true)
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[0])
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(2, conf.getFoo());
        assertEquals(3, conf.getBar());
    }

    @Test
    void completeFlowTestReadFromFileInJarAndEnv() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/integerConfigurationWEnv.json")
                .build();

        Map<String, String> env = new HashMap<>();
        env.put("FOO", "5");

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[0])
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(env)
                .build();

        assertFalse(ctx.getEnvironment().isEmpty());
        assertEquals("5", ctx.getEnvironment().get("FOO"));
        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(5, conf.getFoo());
        assertEquals(3, conf.getBar());
    }


    @Test
    void completeFlowTestReadFromFileInJarAndOverrideWithCliArgs() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/integerConfiguration.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[]{"-f", "6"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(6, conf.getFoo());
        assertEquals(3, conf.getBar());
    }

    @Test
    void completeFlowTestReadFromFileInJarAndEnvAndOverrideWithCliArgs() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/integerConfigurationWEnv.json")
                .build();

        Map<String, String> env = new HashMap<>();
        env.put("FOO", "5");

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[]{"-f", "6"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(env)
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(6, conf.getFoo());
        assertEquals(3, conf.getBar());
    }

    @Test
    void completeFlowTestWithoutFileInJarAndOverrideWithCliArgs() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/nonExistingFile.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[]{"-f", "6"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(6, conf.getFoo());
        assertEquals(0, conf.getBar());
    }

    @Test
    void completeFlowTestReadExternalFile() {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .cliOptionCode("cic")
                .cliOptionName("integerConfiguration")
                .hasCliOption(true)
                .cliOptionDescription("Sample description")
                .inJar(true)
                .path("configuration/nonExistingFile.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(new String[]{"-cic", "testConfiguration.json"})
                .entryPoint(ConfigurationContextFunctionalTests.class)
                .environment(new HashMap<>())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        assertNotNull(conf);
        assertEquals(2, conf.getFoo());
        assertEquals(3, conf.getBar());
    }
}
