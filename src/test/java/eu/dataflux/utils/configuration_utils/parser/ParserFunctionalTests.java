package eu.dataflux.utils.configuration_utils.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import eu.dataflux.utils.configuration_utils.configuration_samples.SampleConfigurations;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.JsonConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.PropertiesConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.XMLConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.YamlConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.YmlConfigurationParser;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserFunctionalTests {

    @SuppressWarnings("unchecked")
    @Test
    void parseJson() {
        IParser parser = new JsonConfigurationParser();
        String sample = "{\"foo\": 2, \"bar\": \"dsa\"}";

        SampleConfigurations.SimpleConfiguration sample1 = parser
                .parseTyped(sample, SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample1.foo);
        assertEquals("dsa", sample1.bar);

        SampleConfigurations.SimpleConfiguration sample2 = (SampleConfigurations.SimpleConfiguration) parser
                .parseTyped(sample, (Class<?>) SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample2.foo);
        assertEquals("dsa", sample2.bar);

        List<SampleConfigurations.SimpleConfiguration> list = (List<SampleConfigurations.SimpleConfiguration>) parser.parse("[" + sample + "]",
                new TypeReference<List<SampleConfigurations.SimpleConfiguration>>() {
                }.getType()
        );

        assertEquals(1, list.size());
        SampleConfigurations.SimpleConfiguration sample3 = list.get(0);
        assertEquals(2, sample3.foo);
        assertEquals("dsa", sample3.bar);
    }

    @Test
    void parseXml() {
        IParser parser = new XMLConfigurationParser();
        String sample = "<SampleConfigurations.SimpleConfiguration><foo>2</foo><bar>dsa</bar></SampleConfigurations.SimpleConfiguration>";

        SampleConfigurations.SimpleConfiguration sample1 = parser
                .parseTyped(sample, SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample1.foo);
        assertEquals("dsa", sample1.bar);

        SampleConfigurations.SimpleConfiguration sample2 = (SampleConfigurations.SimpleConfiguration) parser
                .parseTyped(sample, (Class<?>) SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample2.foo);
        assertEquals("dsa", sample2.bar);
    }

    @Test
    void parseYaml() {
        IParser parser = new YamlConfigurationParser();
        String sample = "foo: 2" + System.lineSeparator() + "bar: \"dsa\"";

        SampleConfigurations.SimpleConfiguration sample1 = parser
                .parseTyped(sample, SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample1.foo);
        assertEquals("dsa", sample1.bar);

        SampleConfigurations.SimpleConfiguration sample2 = (SampleConfigurations.SimpleConfiguration) parser
                .parseTyped(sample, (Class<?>) SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample2.foo);
        assertEquals("dsa", sample2.bar);
    }

    @Test
    void parseYml() {
        IParser parser = new YmlConfigurationParser();
        String sample = "foo: 2" + System.lineSeparator() + "bar: dsa";

        SampleConfigurations.SimpleConfiguration sample1 = parser
                .parseTyped(sample, SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample1.foo);
        assertEquals("dsa", sample1.bar);

        SampleConfigurations.SimpleConfiguration sample2 = (SampleConfigurations.SimpleConfiguration) parser
                .parseTyped(sample, (Class<?>) SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample2.foo);
        assertEquals("dsa", sample2.bar);
    }

    @Test
    void parseProperties() {
        IParser parser = new PropertiesConfigurationParser();
        String sample = "foo=2" + System.lineSeparator() + "bar=dsa";

        SampleConfigurations.SimpleConfiguration sample1 = parser
                .parseTyped(sample, SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample1.foo);
        assertEquals("dsa", sample1.bar);

        SampleConfigurations.SimpleConfiguration sample2 = (SampleConfigurations.SimpleConfiguration) parser
                .parseTyped(sample, (Class<?>) SampleConfigurations.SimpleConfiguration.class);

        assertEquals(2, sample2.foo);
        assertEquals("dsa", sample2.bar);
    }
}
