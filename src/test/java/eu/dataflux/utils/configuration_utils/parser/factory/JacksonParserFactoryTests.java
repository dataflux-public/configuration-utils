package eu.dataflux.utils.configuration_utils.parser.factory;

import eu.dataflux.utils.configuration_utils.parser.factory.impl.JacksonParserFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JacksonParserFactoryTests {

    @Test
    void createJsonParser() {
        IParserFactory unit = new JacksonParserFactory();
        assertNotNull(unit.create("json"));
    }

    @Test
    void createJsonParserWithConfigurator() {
        IParserFactory unit = new JacksonParserFactory(mapper -> mapper);
        assertNotNull(unit.create("json"));
    }

    @Test
    void createPropertiesParser() {
        IParserFactory unit = new JacksonParserFactory();
        assertNotNull(unit.create("properties"));
    }

    @Test
    void createXMLParser() {
        IParserFactory unit = new JacksonParserFactory();
        assertNotNull(unit.create("xml"));
    }

    @Test
    void createYamlParser() {
        IParserFactory unit = new JacksonParserFactory();
        assertNotNull(unit.create("yaml"));
    }

    @Test
    void createYmlParser() {
        IParserFactory unit = new JacksonParserFactory();
        assertNotNull(unit.create("yml"));
    }

    @Test
    void trowIfNotSupported() {
        IParserFactory unit = new JacksonParserFactory();
        assertThrows(IllegalArgumentException.class, () -> unit.create("xsf"));
    }
}
