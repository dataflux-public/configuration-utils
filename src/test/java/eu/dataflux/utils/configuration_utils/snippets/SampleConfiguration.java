package eu.dataflux.utils.configuration_utils.snippets;

import eu.dataflux.utils.configuration_utils.cli.Argument;

import java.util.List;

public class SampleConfiguration {

    @Argument(code = "f", name = "foo", description = "Integer field sample.")
    private Integer foo;

    @Argument(code = "b", name = "bar", description = "Double field sample.")
    private Double bar;

    @Argument(code = "l", name = "list", description = "List field sample.")
    private List<String> list;

    public Integer getFoo() {
        return foo;
    }

    public Double getBar() {
        return bar;
    }

    public List<String> getList() {
        return list;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SampleConfiguration{");
        sb.append("foo=").append(foo);
        sb.append(", bar=").append(bar);
        sb.append(", list=").append(list);
        sb.append('}');
        return sb.toString();
    }
}
