package eu.dataflux.utils.configuration_utils.snippets.parser;

import eu.dataflux.utils.configuration_utils.configuration_samples.SampleConfigurations;
import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.parser.factory.IParserFactory;
import eu.dataflux.utils.configuration_utils.parser.factory.impl.JacksonParserFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigurationParserExample {

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            throw new IllegalArgumentException("You must provide one argument: <configuration_file_path>");
        }

        //read the configuration file from the disk
        String rawConfiguration = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);

        //instantiate parser factory
        IParserFactory parserFactory = new JacksonParserFactory();

        //create IParser implementation based on the file format passed through the cmd
        IParser parser = parserFactory.create(extractFormat(args[0]));

        //parse configuration POJO
        SampleConfigurations.IntegerConfiguration sample =
                parser.parseTyped(rawConfiguration, SampleConfigurations.IntegerConfiguration.class);

        System.out.printf("IntegerConfiguration{foo=%s,bar=%d}\n", sample.getFoo(), sample.getBar());
    }

    private static String extractFormat(String name) {
        int lastDotIndex = name.lastIndexOf('.');
        if (lastDotIndex < 0) return "";

        return name.substring(lastDotIndex + 1);
    }
}
