package eu.dataflux.utils.configuration_utils.snippets.interpolator;

import eu.dataflux.utils.configuration_utils.interpolator.IStringInterpolator;
import eu.dataflux.utils.configuration_utils.interpolator.impl.DefaultStringInterpolator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EnvironmentInterpolationExample {

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            throw new IllegalArgumentException("You must provide one argument: <configuration_file_path>");
        }

        //read the configuration file from the disk
        String rawConfiguration = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);

        //create string interpolator
        IStringInterpolator interpolator = new DefaultStringInterpolator();

        String interpolatedConfiguration = interpolator.interpolate(rawConfiguration, System.getenv());
        System.out.println(interpolatedConfiguration);
    }
}
