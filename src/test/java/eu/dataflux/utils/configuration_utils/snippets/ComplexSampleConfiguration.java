package eu.dataflux.utils.configuration_utils.snippets;

import eu.dataflux.utils.configuration_utils.cli.Argument;
import eu.dataflux.utils.configuration_utils.cli.ComplexArgument;

public class ComplexSampleConfiguration {

    @ComplexArgument
    private SampleConfiguration sample;

    @Argument(code = "fz", name = "faz", description = "String sample.")
    private String faz;

    @Argument(code = "bz", name = "baz", description = "Long sample.")
    private Long baz;

    public SampleConfiguration getSample() {
        return sample;
    }

    public String getFaz() {
        return faz;
    }

    public Long getBaz() {
        return baz;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComplexSampleConfiguration{");
        sb.append("sample=").append(sample);
        sb.append(", faz='").append(faz).append('\'');
        sb.append(", baz=").append(baz);
        sb.append('}');
        return sb.toString();
    }
}
