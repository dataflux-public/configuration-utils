package eu.dataflux.utils.configuration_utils.snippets;

import eu.dataflux.utils.configuration_utils.Configuration;
import eu.dataflux.utils.configuration_utils.ConfigurationContext;
import eu.dataflux.utils.configuration_utils.configuration_samples.SampleConfigurations;

public class ConfigurationContextSample {

    public static void main(String[] args) {
        Configuration intConfiguration = new Configuration.Builder()
                .template(SampleConfigurations.IntegerConfiguration.class)
                .hasCliOption(true)
                .path("configuration/integerConfiguration.json")
                .build();

        ConfigurationContext ctx = new ConfigurationContext.Builder()
                .addConfiguration(intConfiguration)
                .commandLineArguments(args)
                .entryPoint(ConfigurationContextSample.class)
                .environment(System.getenv())
                .build();

        SampleConfigurations.IntegerConfiguration conf =
                ctx.createConfiguration(SampleConfigurations.IntegerConfiguration.class);

        System.out.printf("foo=%d,bar=%d\n", conf.getFoo(), conf.getBar());
    }
}
