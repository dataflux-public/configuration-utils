package eu.dataflux.utils.configuration_utils.snippets.cli;

import eu.dataflux.utils.configuration_utils.Configuration;
import eu.dataflux.utils.configuration_utils.cli.ICommandLine;
import eu.dataflux.utils.configuration_utils.cli.impl.DefaultCommandLine;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.JsonConfigurationParser;
import eu.dataflux.utils.configuration_utils.snippets.ComplexSampleConfiguration;

import java.util.Collections;
import java.util.List;

public class CliSample {

    public static void main(String[] args) {
        ICommandLine cmd = new DefaultCommandLine.Builder()
                .defaultHeader("This is a sample header.") //sets the default header
                .defaultFooter("This is a sample footer.") //sets the default header
                .complexArgumentParser(new JsonConfigurationParser()) //sets the complex argument parser
                .build();                                                //which is used for parsing List and Maps

        Configuration configuration = new Configuration.Builder()
                .template(ComplexSampleConfiguration.class)      //sets the template class which has
                //fields annotated with @Argument or @ComplexArgument
                .path("configuration/sampleConfiguration.json")  //sets the location of the configuration file which
                //is useful if you want to read the configuration
                //before you override it with command line arguments
                .inJar(true)             //tells the ConfigurationContext to search the file inside the jar
                .hasCliOption(true)         //tells the DefaultCommandLine that you want to have the sampleConfiguration
                //location as a CLI argument, which is useful if you have the configuration
                //file outside the jar
                .cliOptionCode("csc")                    //sets the short option for the CLI
                .cliOptionName("sampleConfiguration")    //sets the long option for the CLI
                .cliOptionDescription("SampleConfiguration file location.")  //sets the description of the option
                //for the CLI
                .build();   //builds the Configuration instance

        List<Configuration> configurations = Collections.singletonList(configuration);

        //This must be called before using any ICommandLine instance. It initiates the annotation parsing
        //which results with the CLI that is presented  is passed through the command line
        //arguments. It also initiates the command argument parsing.
        cmd.initialize(CliSample.class, args, configurations);

        if (cmd.isHelpRequested()) {      //checks if the -h/--help option is passed
            cmd.displayHelp();          //displays the CLI
            return;
        }

        ComplexSampleConfiguration sample = new ComplexSampleConfiguration();
        cmd.overrideWithCliOptions(sample);     //sets the fields with the options passed through the command line

        System.out.println(sample);
    }
}
