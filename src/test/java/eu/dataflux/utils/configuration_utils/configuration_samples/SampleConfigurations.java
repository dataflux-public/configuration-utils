package eu.dataflux.utils.configuration_utils.configuration_samples;

import eu.dataflux.utils.configuration_utils.cli.Argument;
import eu.dataflux.utils.configuration_utils.cli.ComplexArgument;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SampleConfigurations {

    public static class SimpleConfiguration {
        public int foo;
        public String bar;
    }

    public static class IntegerConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Integer foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private int bar;

        public Integer getFoo() {
            return foo;
        }

        public int getBar() {
            return bar;
        }
    }

    public static class DoubleConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Double foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private double bar;

        public Double getFoo() {
            return foo;
        }

        public double getBar() {
            return bar;
        }
    }

    public static class LongConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Long foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private long bar;

        public Long getFoo() {
            return foo;
        }

        public long getBar() {
            return bar;
        }
    }

    public static class StringConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private String foo;

        public String getFoo() {
            return foo;
        }
    }

    public static class BooleanConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Boolean foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private boolean bar;

        public Boolean getFoo() {
            return foo;
        }

        public boolean getBar() {
            return bar;
        }
    }

    public static class FloatConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Float foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private float bar;

        public Float getFoo() {
            return foo;
        }

        public float getBar() {
            return bar;
        }
    }

    public static class InnerConfiguration {

        private Integer a;
        private String b;

        public InnerConfiguration() {
            this(null, null);
        }

        public InnerConfiguration(Integer a, String b) {
            this.a = a;
            this.b = b;
        }

        public Integer getA() {
            return a;
        }

        public String getB() {
            return b;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            InnerConfiguration that = (InnerConfiguration) o;
            return Objects.equals(a, that.a) &&
                    Objects.equals(b, that.b);
        }

        @Override
        public int hashCode() {
            return Objects.hash(a, b);
        }
    }

    public static class ListConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private List<String> foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private List<Integer> bar;

        @Argument(code = "c", name = "complex", description = "Complex list args sample.")
        private List<InnerConfiguration> complex;

        public List<String> getFoo() {
            return foo;
        }

        public List<Integer> getBar() {
            return bar;
        }

        public List<InnerConfiguration> getComplex() {
            return complex;
        }
    }

    public static class MapConfiguration {

        @Argument(code = "f", name = "foo", description = "Foo sample.")
        private Map<String, String> foo;

        @Argument(code = "b", name = "bar", description = "Bar sample.")
        private Map<String, Integer> bar;

        @Argument(code = "c", name = "complex", description = "Complex list args sample.")
        private Map<String, InnerConfiguration> complex;

        public Map<String, String> getFoo() {
            return foo;
        }

        public Map<String, Integer> getBar() {
            return bar;
        }

        public Map<String, InnerConfiguration> getComplex() {
            return complex;
        }
    }

    public static class ComplexSampleConfiguration {

        @ComplexArgument
        private IntegerConfiguration integer;

        @Argument(code = "a", name = "aFoo", description = "aFoo sample.")
        private String aFoo;

        @Argument(code = "c.code", name = "c.name", description = "c.description")
        private int cFar;

        public IntegerConfiguration getInteger() {
            return integer;
        }

        public String getaFoo() {
            return aFoo;
        }

        public int getcFar() {
            return cFar;
        }
    }
}
