package eu.dataflux.utils.configuration_utils;

/**
 * This exception is thrown if some exception is thrown while configuration loads.
 */
public class ConfigurationLoadingException extends RuntimeException {

    /**
     * @param message message explaining cause
     */
    public ConfigurationLoadingException(String message) {
        super(message);
    }
}
