package eu.dataflux.utils.configuration_utils.cli;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation describes some configuration field. If the metadata is not used, <code>code</code>,
 * <code>name</code> and <code>description</code> will be used directly. If the metadata file
 * (cli/arguments.properties) is used, <code>code</code>, <code>name</code> and <code>description</code>
 * will represent entry keys, and the real values will be written in the file.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Argument {

    /**
     * @return short option name
     */
    String code();

    /**
     * @return full option name
     */
    String name();

    /**
     * @return option description
     */
    String description();
}
