package eu.dataflux.utils.configuration_utils.cli.impl;

import eu.dataflux.utils.configuration_utils.Configuration;
import eu.dataflux.utils.configuration_utils.cli.CliOption;
import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.JsonConfigurationParser;
import eu.dataflux.utils.configuration_utils.util.PropertiesLoader;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Adapter for Apache Commons CLI library which implements ICommandLine interface.
 */
public class DefaultCommandLine extends AbstractCommandLine {

    private static final String CLI_METADATA_PACKAGE_LOCATION = "cli/metadata.properties";
    private static final String CLI_METADATA_NON_PACKAGE_LOCATION = "metadata.properties";

    private final String defaultHeader;
    private final String defaultFooter;
    private final IParser complexArgumentParser;

    private final CommandLineParser parser;
    private final HelpFormatter helpFormatter;

    private Class<?> entryPoint;
    private CommandLine cmd;
    private Options options;
    private Map<String, String> parsedOptions;
    private boolean ready = false;

    /**
     * Constructor for command line adapter using Apache Commons CLI library.
     *
     * @param defaultHeader         default CLI header
     * @param defaultFooter         default CLI footer
     * @param complexArgumentParser IParser used for parsing complex arguments (lists and maps)
     */
    DefaultCommandLine(String defaultHeader,
                       String defaultFooter,
                       IParser complexArgumentParser) {
        this.defaultHeader = defaultHeader;
        this.defaultFooter = defaultFooter;
        this.complexArgumentParser = complexArgumentParser;


        this.parser = new DefaultParser();
        this.helpFormatter = new HelpFormatter();
    }

    @Override
    public boolean isHelpRequested() {
        if (!isReady()) {
            throw new IllegalStateException(
                    "Method isHelpRequested cannot be called. Call initialize first.");
        }

        return cmd.hasOption("h");
    }

    @Override
    public void displayHelp() {
        if (!isReady()) {
            throw new IllegalStateException(
                    "Method displayHelp cannot be called. Call initialize first.");
        }

        Properties meta = PropertiesLoader.load(
                CLI_METADATA_PACKAGE_LOCATION,
                CLI_METADATA_NON_PACKAGE_LOCATION
        );

        String header = meta.getProperty("header", defaultHeader);
        String footer = meta.getProperty("footer", defaultFooter);

        helpFormatter.printHelp(
                120,
                entryPoint.getName(),
                header,
                options,
                footer,
                true
        );
    }

    @Override
    public void initialize(Class<?> entryPoint, String[] args, List<Configuration> configurations) {
        Objects.requireNonNull(entryPoint);
        Objects.requireNonNull(args);
        Objects.requireNonNull(configurations);

        this.entryPoint = entryPoint;
        this.options = createOptions(
                createCliOptionsFromConfiguration(
                        configurations.stream()
                                .map(Configuration::getTemplate)
                                .collect(Collectors.toList())
                ), configurations.stream()
                        .map(Configuration::getConfigurationFileLocationCliOption)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList())
        );
        this.cmd = createCmd(args);
        this.parsedOptions = parseArgs();
        this.ready = true;
    }

    private Options createOptions(List<CliOption> configurationOptions, List<CliOption> configurationLocationOptions) {
        final Options options = new Options();

        Stream.concat(configurationOptions.stream(), configurationLocationOptions.stream())
                .map(cliOption -> Option.builder(cliOption.getCode())
                        .longOpt(cliOption.getName())
                        .desc(cliOption.getDescription())
                        .required(false)
                        .hasArg(true)
                        .build()
                )
                .forEach(options::addOption);

        options.addOption(
                Option.builder("h")
                        .longOpt("help")
                        .desc("Preview help")
                        .required(false)
                        .hasArg(false)
                        .build()
        );

        return options;
    }

    private CommandLine createCmd(String[] commandLineArguments) {
        try {
            return parser.parse(options, commandLineArguments);
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex.getMessage(), ex);
        }
    }

    private Map<String, String> parseArgs() {
        Map<String, String> optionsMap = new HashMap<>();

        for (Option o : cmd.getOptions()) {
            String value = o.getValue();
            if (value != null)
                optionsMap.put(o.getOpt(), value);
        }

        return optionsMap;
    }

    @Override
    public Map<String, String> getParsedOptions() {
        if (!isReady()) {
            throw new IllegalStateException(
                    "Method getParsedOptions cannot be called. Call initialize first.");
        }

        return parsedOptions;
    }

    @Override
    protected boolean isReady() {
        return ready;
    }

    @Override
    protected IParser getComplexArgumentParser() {
        return complexArgumentParser;
    }

    /**
     * Builder class for <code>DefaultCommandLine</code>
     */
    public static class Builder {

        private String defaultHeader = "";
        private String defaultFooter = "";
        private IParser complexArgumentParser = new JsonConfigurationParser();

        /**
         * Sets default header for the CLI. It's empty string by default.
         *
         * @param defaultHeader default header for the CLI
         * @return builder
         */
        public Builder defaultHeader(String defaultHeader) {
            this.defaultHeader = defaultHeader;
            return this;
        }

        /**
         * Sets default footer for the CLI. It's empty string by default.
         *
         * @param defaultFooter default footer for the CLI
         * @return builder
         */
        public Builder defaultFooter(String defaultFooter) {
            this.defaultFooter = defaultFooter;
            return this;
        }

        /**
         * Sets the implementation of <code>IParser</code> which will be used for parsing
         * List and Map type options. By default, <code>JsonConfigurationParser</code> is used.
         *
         * @param complexArgumentParser parser for complex arguments
         * @return builder
         */
        public Builder complexArgumentParser(IParser complexArgumentParser) {
            this.complexArgumentParser = complexArgumentParser;
            return this;
        }

        /**
         * @return instance of <code>DefaultCommandLine</code> based on the builder arguments
         */
        public DefaultCommandLine build() {
            if (!areArgumentsSet()) {
                throw new IllegalStateException("Builder is not ready, all arguments must not be null.");
            }

            return new DefaultCommandLine(
                    defaultHeader,
                    defaultFooter,
                    complexArgumentParser
            );
        }

        private boolean areArgumentsSet() {
            return defaultFooter != null &&
                    defaultHeader != null &&
                    complexArgumentParser != null;
        }
    }
}
