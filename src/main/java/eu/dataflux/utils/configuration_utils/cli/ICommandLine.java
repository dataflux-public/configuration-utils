package eu.dataflux.utils.configuration_utils.cli;

import eu.dataflux.utils.configuration_utils.Configuration;

import java.util.List;
import java.util.Map;

/**
 * This interface defines a basic functions of command line interface which is necessary for handling command
 * line arguments.
 */
public interface ICommandLine {

    /**
     * This method must be called before using all other methods. It passes command line arguments to inner
     * library which initiates argument parsing.
     *
     * @param entryPoint     entry point of the program (class with the main method)
     * @param args           command line arguments
     * @param configurations list of Configuration class instances
     */
    void initialize(Class<?> entryPoint, String[] args, List<Configuration> configurations);

    /**
     * @return key-value pairs parsed from command line arguments.
     */
    Map<String, String> getParsedOptions();

    /**
     * @param conf configuration object
     * @return configuration object with fields overridden by CLI options
     */
    Object overrideWithCliOptions(Object conf);

    /**
     * @param configuration requested <code>Configuration</code> instance
     * @return path passed as a command line argument, <code>null</code> otherwise
     */
    String getConfigurationFileLocation(Configuration configuration);

    /**
     * @return true if help was requested, false otherwise
     */
    boolean isHelpRequested();

    /**
     * Prints help text on the System.out
     */
    void displayHelp();
}
