package eu.dataflux.utils.configuration_utils.cli.impl;

import eu.dataflux.utils.configuration_utils.Configuration;
import eu.dataflux.utils.configuration_utils.cli.Argument;
import eu.dataflux.utils.configuration_utils.cli.CliOption;
import eu.dataflux.utils.configuration_utils.cli.ComplexArgument;
import eu.dataflux.utils.configuration_utils.cli.ICommandLine;
import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.util.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * This class represents a base implementation of ICommandLine interface. It holds the logic for
 * reading the documentation of each field and overriding values of the configuration classes
 * instances.
 */
public abstract class AbstractCommandLine implements ICommandLine {

    private static final String ARGS_METADATA_PACKAGE_LOCATION = "cli/arguments.properties";
    private static final String ARGS_METADATA_NON_PACKAGE_LOCATION = "arguments.properties";

    private final Logger logger = LoggerFactory.getLogger(AbstractCommandLine.class);

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    protected abstract boolean isReady();

    @Override
    public String getConfigurationFileLocation(Configuration configuration) {
        if (!isReady()) {
            throw new IllegalStateException(
                    "Method getConfigurationFileLocation cannot be called. Call initialize first.");
        }

        CliOption option = configuration.getConfigurationFileLocationCliOption();
        if (option == null) {
            return null;
        }

        return getParsedOptions().getOrDefault(option.getCode(), null);
    }

    @Override
    public Object overrideWithCliOptions(Object conf) {
        if (!isReady()) {
            throw new IllegalStateException(
                    "Method overrideWithCliOptions cannot be called. Call initialize first.");
        }

        return overrideWithCliOptionsInternal(conf, getParsedOptions(), loadArgumentsMetadata());
    }

    private Object overrideWithCliOptionsInternal(Object conf,
                                                  Map<String, String> options,
                                                  Properties argumentsMetadata) {
        Class<?> refCls = conf.getClass();

        for (Field field : refCls.getDeclaredFields()) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(ComplexArgument.class)) {
                setComplexArgument(conf, field, options, argumentsMetadata);
            } else if (field.isAnnotationPresent(Argument.class)) {
                Argument argument = field.getAnnotation(Argument.class);
                String line = options.get(
                        argumentsMetadata.getProperty(argument.code(), argument.code())
                );
                if (line == null) continue;

                setArgument(conf, field, line);
            }
        }

        return conf;
    }

    private Properties loadArgumentsMetadata() {
        return PropertiesLoader.load(
                ARGS_METADATA_PACKAGE_LOCATION,
                ARGS_METADATA_NON_PACKAGE_LOCATION
        );
    }

    private void setComplexArgument(Object conf,
                                    Field field,
                                    Map<String, String> options,
                                    Properties argumentsMetadata) {
        try {
            Object object = field.get(conf) != null ? field.get(conf) : field.getType().newInstance();
            setObjectToField(field, conf, overrideWithCliOptionsInternal(object, options, argumentsMetadata));
        } catch (InstantiationException e) {
            logger.warn(String.format("Class %s cannot be instantiated.", field.getDeclaringClass().getName()), e);
        } catch (IllegalAccessException ignored) {
        }
    }

    @SuppressWarnings("UnnecessaryBoxing")
    private void setArgument(Object conf, Field field, String line) {
        Class<?> fieldCls = field.getType();

        try {
            if (fieldCls.equals(Integer.TYPE) || fieldCls.equals(Integer.class)) {
                int value = Integer.parseInt(line);

                if (fieldCls.equals(Integer.TYPE)) {
                    field.setInt(conf, value);
                } else {
                    setObjectToField(field, conf, Integer.valueOf(value));
                }
            } else if (fieldCls.equals(Double.TYPE) || fieldCls.equals(Double.class)) {
                double value = Double.parseDouble(line);

                if (fieldCls.equals(Double.TYPE)) {
                    field.setDouble(conf, value);
                } else {
                    setObjectToField(field, conf, Double.valueOf(value));
                }
            } else if (fieldCls.equals(Long.TYPE) || fieldCls.equals(Long.class)) {
                long value = Long.parseLong(line);

                if (fieldCls.equals(Long.TYPE)) {
                    field.setLong(conf, value);
                } else {
                    setObjectToField(field, conf, Long.valueOf(value));
                }
            } else if (fieldCls.equals(Boolean.TYPE) || fieldCls.equals(Boolean.class)) {
                boolean value = Boolean.parseBoolean(line);

                if (fieldCls.equals(Boolean.TYPE)) {
                    field.setBoolean(conf, value);
                } else {
                    setObjectToField(field, conf, Boolean.valueOf(value));
                }
            } else if (fieldCls.equals(Float.TYPE) || fieldCls.equals(Float.class)) {
                float value = Float.parseFloat(line);

                if (fieldCls.equals(Float.TYPE)) {
                    field.setFloat(conf, value);
                } else {
                    setObjectToField(field, conf, Float.valueOf(value));
                }
            } else if (fieldCls == String.class) {
                setObjectToField(field, conf, line);
            } else if (fieldCls == List.class || fieldCls == Map.class) {
                setObjectToField(field, conf, getComplexArgumentParser().parse(line, field.getGenericType()));
            }
        } catch (NumberFormatException | IllegalAccessException ignored) {
            logger.warn(String.format("%s is not supported value for the field %s. Skipping...", fieldCls.getName(), field.getName()));
        }
    }

    private void setObjectToField(Field field, Object object, Object value) {
        try {
            field.set(object, value);
        } catch (IllegalAccessException e) {
            logger.warn(String.format("Field %s cannot be set.", field.getName()), e);
        }
    }

    protected abstract IParser getComplexArgumentParser();

    protected List<CliOption> createCliOptionsFromConfiguration(List<Class<?>> classes) {
        Map<String, CliOption> options = new HashMap<>();
        Properties argumentsMetadata = loadArgumentsMetadata();

        for (Class<?> cls : classes) {
            createCliOptionsInternal(cls, options, argumentsMetadata);
        }

        return new ArrayList<>(options.values());
    }

    private void createCliOptionsInternal(Class<?> cls,
                                          Map<String, CliOption> options,
                                          Properties argumentsMetadata) {
        for (Field field : cls.getDeclaredFields()) {
            if (field.isAnnotationPresent(ComplexArgument.class)) {
                createCliOptionsInternal(field.getType(), options, argumentsMetadata);
            } else if (field.isAnnotationPresent(Argument.class)) {
                Argument argument = field.getAnnotation(Argument.class);

                String code = argumentsMetadata.getProperty(argument.code(), argument.code());
                if (options.containsKey(code)) {
                    throw new IllegalArgumentException(String.format("There are multiple options named %s", code));
                }

                String name = argumentsMetadata.getProperty(argument.name(), argument.name());
                String description = argumentsMetadata.getProperty(argument.description(), argument.description());

                options.put(code, new CliOption(code, name, description));
            }
        }
    }
}
