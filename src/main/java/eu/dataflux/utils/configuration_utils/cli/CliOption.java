package eu.dataflux.utils.configuration_utils.cli;

/**
 * POJO class which uses as descriptor of an command line option which can be used for modifying some configurations.
 */
public class CliOption {

    private final String code;
    private final String name;
    private final String description;

    /**
     * @param code        short option name
     * @param name        full option name
     * @param description option description
     */
    public CliOption(String code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    /**
     * @return short option name
     */
    public String getCode() {
        return code;
    }

    /**
     * @return full option name
     */
    public String getName() {
        return name;
    }

    /**
     * @return option description
     */
    public String getDescription() {
        return description;
    }
}
