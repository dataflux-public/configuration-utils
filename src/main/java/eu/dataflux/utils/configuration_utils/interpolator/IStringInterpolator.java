package eu.dataflux.utils.configuration_utils.interpolator;

import java.util.Map;

/**
 * This interface defines an object which is used for interpolating variables in
 * text.
 */
public interface IStringInterpolator {

    /**
     * This method is used for interpolating the variables by key into raw text.
     *
     * @param text      raw text
     * @param variables Map of (variable name - variable value) pairs (case sensitive)
     * @return interpolated text
     */
    String interpolate(String text, Map<String, String> variables);
}
