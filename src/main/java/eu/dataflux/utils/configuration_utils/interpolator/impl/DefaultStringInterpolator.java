package eu.dataflux.utils.configuration_utils.interpolator.impl;

import eu.dataflux.utils.configuration_utils.interpolator.IStringInterpolator;
import org.apache.commons.text.StringSubstitutor;

import java.util.Map;

/**
 * This class represents the default <code>IStringInterpolator</code> which
 * acts a wrapper around <code>StringSubstitutor</code> class (defined
 * in Apache Commons Text). The default definition of a variable is ${variableName}.
 * The default value of the variable default value delimiter is :-, as in bash and other *nix shells.
 */
public class DefaultStringInterpolator implements IStringInterpolator {

    @Override
    public String interpolate(String text, Map<String, String> variables) {
        StringSubstitutor substitutor = new StringSubstitutor(variables);
        return substitutor.replace(text);
    }
}
