package eu.dataflux.utils.configuration_utils;

import eu.dataflux.utils.configuration_utils.cli.CliOption;

/**
 * This class acts as a descriptor of some configuration file.
 */
public class Configuration {

    private final Class<?> template;
    private final String path;
    private final boolean inJar;
    private final CliOption configurationFileLocationCliOption;

    Configuration(Class<?> template,
                  String path,
                  boolean inJar,
                  CliOption configurationFileLocationCliOption) {
        this.template = template;
        this.path = path;
        this.inJar = inJar;
        this.configurationFileLocationCliOption = configurationFileLocationCliOption;
    }

    Configuration(Class<?> template,
                  String path,
                  boolean inJar) {
        this(template, path, inJar, null);
    }

    /**
     * @return template class
     */
    public Class<?> getTemplate() {
        return template;
    }

    /**
     * @return path of the configuration file
     */
    public String getPath() {
        return path;
    }

    /**
     * @return <code>true</code> if the configuration file is inside of jar package,
     * <code>false</code> otherwise
     */
    public boolean isInJar() {
        return inJar;
    }

    /**
     * @return instance of <code>CliOption</code> if configuration file location option is used,
     * <code>null</code> otherwise.
     */
    public CliOption getConfigurationFileLocationCliOption() {
        return configurationFileLocationCliOption;
    }

    /**
     * Builder class for <code>Configuration</code> objects.
     */
    public static class Builder {

        private Class<?> template;
        private String path;
        private boolean inJar = true;
        private String code;
        private String name;
        private String description;
        private boolean hasCliOption = false;

        /**
         * @param template template class
         * @return builder
         */
        public Builder template(Class<?> template) {
            this.template = template;
            return this;
        }

        /**
         * @param path path of the configuration file
         * @return builder
         */
        public Builder path(String path) {
            this.path = path;
            return this;
        }

        /**
         * @param inJar <code>true</code> if the configuration file is inside of jar package,
         *              <code>false</code> otherwise
         * @return builder
         */
        public Builder inJar(boolean inJar) {
            this.inJar = inJar;
            return this;
        }

        /**
         * @param hasCliOption <code>true</code> if the cli option for configuration location is used,
         *                     <code>false</code> otherwise
         * @return builder
         */
        public Builder hasCliOption(boolean hasCliOption) {
            this.hasCliOption = hasCliOption;
            return this;
        }

        /**
         * @param code short name for the cli option for configuration location
         * @return builder
         */
        public Builder cliOptionCode(String code) {
            this.code = code;
            return this;
        }

        /**
         * @param name full name for the cli option for configuration location
         * @return builder
         */
        public Builder cliOptionName(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param description description for the cli option for configuration location
         * @return builder
         */
        public Builder cliOptionDescription(String description) {
            this.description = description;
            return this;
        }

        /**
         * Builds the new <code>Configuration</code> instance based on the builder arguments
         *
         * @return new <code>Configuration</code> instance
         */
        public Configuration build() {
            if (!areArgumentsSet()) {
                throw new IllegalArgumentException("All arguments must be set.");
            }

            path = path == null ? generateFileLocation() : path;
            if (hasCliOption) {
                if (code == null) {
                    code = generateCliOptionCode();
                }

                if (name == null) {
                    name = generateCliOptionName();
                }

                if (description == null) {
                    description = generateCliOptionDescription();
                }

                return new Configuration(template, path, inJar, new CliOption(code, name, description));
            } else {
                return new Configuration(template, path, inJar);
            }
        }

        private boolean areArgumentsSet() {
            return template != null;
        }

        private String generateCliOptionCode() {
            String uppercaseChars = template
                    .getSimpleName()
                    .chars()
                    .filter(Character::isUpperCase)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();

            return String.format("c%s", uppercaseChars.toLowerCase());
        }

        private String generateCliOptionName() {
            return toCamelCase(String.format("%sLocation", template.getSimpleName()));
        }

        private String generateFileLocation() {
            return String.format("%s.json", toCamelCase(template.getSimpleName()));
        }

        private String toCamelCase(String name) {
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }

        private String generateCliOptionDescription() {
            return String.format(
                    "Location of %s configuration file. `%s` file %s will be read by default.",
                    template.getSimpleName(), path, inJar ? "from jar" : "from file system");
        }
    }
}
