package eu.dataflux.utils.configuration_utils;

import eu.dataflux.utils.configuration_utils.cli.ICommandLine;
import eu.dataflux.utils.configuration_utils.cli.impl.DefaultCommandLine;
import eu.dataflux.utils.configuration_utils.interpolator.IStringInterpolator;
import eu.dataflux.utils.configuration_utils.interpolator.impl.DefaultStringInterpolator;
import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.parser.factory.IParserFactory;
import eu.dataflux.utils.configuration_utils.parser.factory.impl.JacksonParserFactory;
import eu.dataflux.utils.configuration_utils.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * This class acts as a facade for <code>IParser</code>, <code>ICommandLine</code> and <code>IStringInterpolator</code>.
 * It combines the functionalities of those three APIs to create the configuration(s) combined
 * from multiple sources.
 */
public class ConfigurationContext {

    private final Logger logger = LoggerFactory.getLogger(ConfigurationContext.class);

    private final List<Configuration> configurations;
    private final Map<String, String> environment;
    private final IParserFactory parserFactory;
    private final IStringInterpolator stringInterpolator;
    private final ICommandLine commandLine;

    ConfigurationContext(List<Configuration> configurations,
                         Map<String, String> environment,
                         IParserFactory parserFactory,
                         IStringInterpolator stringInterpolator,
                         ICommandLine commandLine) {
        this.configurations = configurations;
        this.environment = environment;
        this.parserFactory = parserFactory;
        this.stringInterpolator = stringInterpolator;
        this.commandLine = commandLine;
    }

    /**
     * @return list of <code>Configuration</code> objects passed to constructor
     * which act as the templates for the POJOs which will be created by the
     * <code>ConfigurationContext::createConfiguration(Class) </code>
     * method
     */
    public List<Configuration> getConfigurations() {
        return configurations;
    }

    /**
     * @return environment passed to the constructor
     */
    public Map<String, String> getEnvironment() {
        return environment;
    }

    /**
     * @return <code>ICommandLine</code> instance passed to the constructor
     */
    public ICommandLine getCommandLine() {
        return commandLine;
    }

    /**
     * Creates the configuration based on the template which is of type passed
     * as a parameter.
     *
     * @param type type reference
     * @param <T>  type of the created POJO configuration
     * @return POJO configuration
     */
    public <T> T createConfiguration(Class<T> type) {
        Configuration configuration = configurations.stream()
                .filter(x -> x.getTemplate() == type)
                .findFirst()
                .orElseThrow(() -> new ConfigurationLoadingException(
                        String.format("Configuration with template %s cannot be found.", type.getName()))
                );

        String cliPath = commandLine.getConfigurationFileLocation(configuration);
        String rawConfiguration, format;
        T configurationInstance;

        try {
            if (cliPath == null) {
                rawConfiguration = read(configuration.getPath(), configuration.isInJar());
                format = extractFormat(configuration.getPath());
            } else {
                rawConfiguration = read(cliPath, false);
                format = extractFormat(cliPath);
            }

            IParser parser = parserFactory.create(format);
            String interpolatedConfiguration = stringInterpolator.interpolate(rawConfiguration, environment);
            configurationInstance = parser.parseTyped(interpolatedConfiguration, type);
        } catch (Exception ex) {
            try {
                configurationInstance = type.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalArgumentException(
                        "Configuration class cannot be instantiated: " + type.getName(), e);
            }
        }

        commandLine.overrideWithCliOptions(configurationInstance);
        return configurationInstance;
    }

    private String extractFormat(String name) {
        int lastDotIndex = name.lastIndexOf('.');
        if (lastDotIndex < 0) return "";

        return name.substring(lastDotIndex + 1);
    }

    private String read(String path, boolean fromJar) {
        try {
            if (fromJar) {
                URL resource = ClassLoader.getSystemClassLoader().getResource(path);

                return new String(
                        IOUtils.readAllBytes(Objects.requireNonNull(resource).openStream())
                        , StandardCharsets.UTF_8
                );
            } else {
                return new String(
                        Files.readAllBytes(Paths.get(path)),
                        StandardCharsets.UTF_8);
            }
        } catch (Exception ex) {
            logger.warn("File {} cannot be read.", path, ex);
            return null;
        }
    }

    /**
     * Builder class for <code>ConfigurationContext</code>.
     */
    public static class Builder {
        private Class<?> entryPoint;
        private String[] commandLineArguments;
        private final List<Configuration> configurations = new ArrayList<>();
        private Map<String, String> environment = System.getenv();
        private IParserFactory parserFactory = new JacksonParserFactory();
        private IStringInterpolator stringInterpolator = new DefaultStringInterpolator();
        private ICommandLine commandLine;

        /**
         * @param commandLine instance of the <code>ICommandLine</code> interface
         * @return builder
         */
        public Builder commandLine(ICommandLine commandLine) {
            this.commandLine = commandLine;
            return this;
        }

        /**
         * @param entryPoint class with the <code>main</code> method
         * @return builder
         */
        public Builder entryPoint(Class<?> entryPoint) {
            this.entryPoint = entryPoint;
            return this;
        }

        /**
         * @param commandLineArguments command line arguments
         * @return builder
         */
        public Builder commandLineArguments(String[] commandLineArguments) {
            this.commandLineArguments = commandLineArguments;
            return this;
        }

        /**
         * @param configuration POJO configuration template
         * @return builder
         */
        public Builder addConfiguration(Configuration configuration) {
            this.configurations.add(configuration);
            return this;
        }

        /**
         * @param environment environment variables
         * @return builder
         */
        public Builder environment(Map<String, String> environment) {
            this.environment = environment;
            return this;
        }

        /**
         * @param parserFactory <code>IParserFactory</code> instance used for parsing POJO configurations
         * @return builder
         */
        public Builder parserFactory(IParserFactory parserFactory) {
            this.parserFactory = parserFactory;
            return this;
        }

        /**
         * @param stringInterpolator <code>IParserFactory</code> instance used for environment variables interpolation
         * @return builder
         */
        public Builder stringInterpolator(IStringInterpolator stringInterpolator) {
            this.stringInterpolator = stringInterpolator;
            return this;
        }

        /**
         * Creates the instance of <code>ConfigurationContext</code> based on the builder arguments
         *
         * @return instance of <code>ConfigurationContext</code>
         */
        public ConfigurationContext build() {
            if (!areArgumentsSet()) {
                throw new IllegalArgumentException("All arguments must be set.");
            }

            if (commandLine == null) {
                commandLine = new DefaultCommandLine.Builder().build();
            }
            commandLine.initialize(entryPoint, commandLineArguments, configurations);

            return new ConfigurationContext(configurations, environment, parserFactory, stringInterpolator, commandLine);
        }

        private boolean areArgumentsSet() {
            return environment != null &&
                    parserFactory != null &&
                    stringInterpolator != null &&
                    commandLineArguments != null &&
                    entryPoint != null;
        }
    }
}
