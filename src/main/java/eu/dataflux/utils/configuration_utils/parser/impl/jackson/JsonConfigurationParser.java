package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;

/**
 * Configuration parser for JSON format using Jackson's <code>{@link ObjectMapper}</code>.
 */
public class JsonConfigurationParser extends AbstractJacksonParser {

    private static final String FORMAT = "json";

    public JsonConfigurationParser(IObjectMapperConfigurator configurator) {
        super(new ObjectMapper(), FORMAT, configurator);
    }

    public JsonConfigurationParser() {
        this(null);
    }
}
