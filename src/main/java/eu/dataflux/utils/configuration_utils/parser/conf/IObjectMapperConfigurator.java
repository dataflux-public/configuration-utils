package eu.dataflux.utils.configuration_utils.parser.conf;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This interface defines configurator class for <code>{@link ObjectMapper}</code> which will be used to parse
 * the configuration files into POJOs.
 */
public interface IObjectMapperConfigurator {

    /**
     * @param mapper object to be configured
     * @return configured object
     */
    ObjectMapper configure(ObjectMapper mapper);
}
