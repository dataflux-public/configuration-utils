package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;

/**
 * Configuration parser for XML format using Jackson's <code>{@link XmlMapper}</code>.
 */
public class XMLConfigurationParser extends AbstractJacksonParser {

    private static final String FORMAT = "xml";

    public XMLConfigurationParser(IObjectMapperConfigurator configurator) {
        super(new XmlMapper(), FORMAT, configurator);
    }

    public XMLConfigurationParser() {
        this(null);
    }
}
