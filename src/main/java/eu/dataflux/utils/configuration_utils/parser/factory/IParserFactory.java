package eu.dataflux.utils.configuration_utils.parser.factory;

import eu.dataflux.utils.configuration_utils.parser.IParser;

/**
 * This interface defines an object used for creating the <code>IParser</code>
 * instances by the format which can be retrieved by calling the <code>IParser::format()</code>.
 */
public interface IParserFactory {

    /**
     * This method creates the <code>IParser</code>.
     *
     * @param format format retrieved by <code>IParser::format()</code>
     * @return new instance of <code>IParser</code>, null if there's no implementation for that format
     */
    IParser create(String format);
}
