package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;

/**
 * Configuration parser for YML/YAML format using Jackson's <code>{@link YAMLMapper}</code>.
 */
public class YamlConfigurationParser extends AbstractJacksonParser {

    private static final String FORMAT = "yaml";

    public YamlConfigurationParser(IObjectMapperConfigurator configurator) {
        super(new YAMLMapper(), FORMAT, configurator);
    }

    public YamlConfigurationParser() {
        this(null);
    }
}
