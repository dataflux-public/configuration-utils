package eu.dataflux.utils.configuration_utils.parser;

import java.lang.reflect.Type;

/**
 * This interface defines an object which creates the object of some class/type from the
 * text.
 */
public interface IParser {

    /**
     * Creates an instance of passed class from provided string.
     *
     * @param text string of the format defined by <code>IParser::format</code> method
     * @param type class of the parsed instance
     * @return parsed instance, null if text cannot be parsed
     */
    Object parse(String text, Class<?> type);

    /**
     * Creates an instance of passed class from provided string.
     *
     * @param text string of the format defined by <code>IParser::format</code> method
     * @param type class of the parsed instance
     * @return parsed instance, null if text cannot be parsed
     */
    <T> T parseTyped(String text, Class<T> type);

    /**
     * Creates an instance of passed class from provided string. This method can be used
     * for parsing instances of parametrized types.
     *
     * @param text string of the format defined by <code>IParser::format</code> method
     * @param type class of the parsed instance
     * @return parsed instance, null if text cannot be parsed
     */
    Object parse(String text, Type type);

    /**
     * @return lower-cased format name (extension without .)
     */
    String format();
}
