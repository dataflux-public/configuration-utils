package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * This is an abstract implementation of <code>IParser</code> interface common to all implementations
 * which rely on the Jackson serializer library.
 */
public abstract class AbstractJacksonParser implements IParser {

    private final Logger logger = LoggerFactory.getLogger(AbstractJacksonParser.class);

    private final ObjectMapper parser;
    private final String format;

    protected AbstractJacksonParser(ObjectMapper parser, String format, IObjectMapperConfigurator configurator) {
        Objects.requireNonNull(parser, format);
        this.parser = configurator == null ? parser : configurator.configure(parser);
        this.format = format;
    }

    @Override
    public Object parse(String text, Type type) {
        try {
            return parser.readValue(text, new TypeReferenceDirect(type));
        } catch (JsonProcessingException ex) {
            logger.warn(String.format("%s cannot be parsed to %s", text, type.getTypeName()), ex);
            return null;
        }
    }

    @Override
    public Object parse(String text, Class<?> type) {
        try {
            return parser.readValue(text, type);
        } catch (JsonProcessingException ex) {
            logger.warn(String.format("%s cannot be parsed to %s", text, type.getTypeName()), ex);
            return null;
        }
    }

    @Override
    public <T> T parseTyped(String text, Class<T> type) {
        try {
            return parser.readValue(text, type);
        } catch (JsonProcessingException ex) {
            logger.warn(String.format("%s cannot be parsed to %s", text, type.getTypeName()), ex);
            return null;
        }
    }

    @Override
    public String format() {
        return format;
    }

    private static class TypeReferenceDirect extends TypeReference<Object> {

        private final Type type;

        public TypeReferenceDirect(Type type) {
            this.type = type;
        }

        @Override
        public Type getType() {
            return type;
        }
    }
}
