package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;

/**
 * Configuration parser for Properties format using Jackson's <code>{@link JavaPropsMapper}</code>.
 */
public class PropertiesConfigurationParser extends AbstractJacksonParser {

    private static final String FORMAT = "properties";

    public PropertiesConfigurationParser(IObjectMapperConfigurator configurator) {
        super(new JavaPropsMapper(), FORMAT, configurator);
    }

    public PropertiesConfigurationParser() {
        this(null);
    }
}
