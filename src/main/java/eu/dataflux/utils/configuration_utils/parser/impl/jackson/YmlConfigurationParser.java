package eu.dataflux.utils.configuration_utils.parser.impl.jackson;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;

/**
 * Configuration parser for YML format using Jackson's <code>{@link YAMLMapper}</code>. This
 * implementation is the same as <code>YmlConfigurationParser</code> but because of variation for
 * the Yet Another Markup Language format.
 */
public class YmlConfigurationParser extends AbstractJacksonParser {

    private static final String FORMAT = "yml";

    public YmlConfigurationParser(IObjectMapperConfigurator configurator) {
        super(new YAMLMapper(), FORMAT, configurator);
    }

    public YmlConfigurationParser() {
        this(null);
    }
}
