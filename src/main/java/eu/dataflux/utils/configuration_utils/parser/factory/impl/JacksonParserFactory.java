package eu.dataflux.utils.configuration_utils.parser.factory.impl;

import eu.dataflux.utils.configuration_utils.parser.IParser;
import eu.dataflux.utils.configuration_utils.parser.conf.IObjectMapperConfigurator;
import eu.dataflux.utils.configuration_utils.parser.factory.IParserFactory;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.JsonConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.PropertiesConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.XMLConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.YamlConfigurationParser;
import eu.dataflux.utils.configuration_utils.parser.impl.jackson.YmlConfigurationParser;

import java.util.HashMap;
import java.util.Map;

/**
 * This implementation of <code>IParserFactory</code> creates instances of <code>IParser</code>
 * interface implemented within the <code>eu.dataflux.utils.configuration_utils.parser.impl.jackson</code>
 * package.
 */
public class JacksonParserFactory implements IParserFactory {

    private final Map<String, IParser> parserMap = new HashMap<>();

    public JacksonParserFactory(IObjectMapperConfigurator configurator) {
        addParser(new JsonConfigurationParser(configurator));
        addParser(new YamlConfigurationParser(configurator));
        addParser(new YmlConfigurationParser(configurator));
        addParser(new PropertiesConfigurationParser(configurator));
        addParser(new XMLConfigurationParser(configurator));
    }

    private void addParser(IParser parser) {
        parserMap.put(parser.format(), parser);
    }

    public JacksonParserFactory() {
        this(null);
    }

    @Override
    public IParser create(String format) {
        if (parserMap.containsKey(format)) {
            return parserMap.get(format);
        } else {
            throw new IllegalArgumentException(
                    String.format("IParser instance for given format (%s) isn't supported.", format)
            );
        }
    }
}
