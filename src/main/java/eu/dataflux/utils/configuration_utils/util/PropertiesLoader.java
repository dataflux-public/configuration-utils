package eu.dataflux.utils.configuration_utils.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

/**
 * Utility class used for loading Properties object from package or outside of the package
 */
public abstract class PropertiesLoader {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

    /**
     * This method tries to load Properties object from package if possible, otherwise it will
     * load Properties from the outside of the package.
     *
     * @param packageLocation    location from inside of the package
     * @param nonPackageLocation location from outside of the package
     * @return Properties object
     */
    public static Properties load(String packageLocation, String nonPackageLocation) {
        Properties argumentsMetadata = new Properties();

        try {
            Enumeration<URL> resEnum = ClassLoader.getSystemClassLoader().getResources(packageLocation);
            List<URL> resources = Collections.list(resEnum);

            if (resources.size() != 0) {
                for (URL url : resources) {
                    Properties next = new Properties();
                    next.load(url.openStream());

                    argumentsMetadata.putAll(next);
                }
            } else {
                argumentsMetadata.load(new FileInputStream(nonPackageLocation));
            }
        } catch (IOException ex) {
            logger.warn("Cannot read file.", ex);
        }

        return argumentsMetadata;
    }
}
